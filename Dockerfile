FROM node:10.15.3-alpine

# Create app directory
WORKDIR /usr/src/app

# Copy app source
COPY . .

# Install production node modules
RUN npm ci --only=production

# Switch to non root user
USER node

# Expose ports
EXPOSE 8080

# Run the app
CMD [ "node", "app.js" ]