'use strict';

const path = require('path');
const fs = require('fs');
const express = require('express');
const router = express.Router();

const HEADERS = {
	// FormatHeader name of the header used to extract the format
	FormatHeader: 'X-Format',

	// CodeHeader name of the header used as source of the HTTP status code to return
	CodeHeader: 'X-Code',

	// ContentType name of the header that defines the format of the reply
	ContentType: 'Content-Type',

	// OriginalURI name of the header with the original URL from NGINX
	OriginalURI: 'X-Original-URI',

	// Namespace name of the header that contains information about the Ingress namespace
	Namespace: 'X-Namespace',

	// IngressName name of the header that contains the matched Ingress
	IngressName: 'X-Ingress-Name',

	// ServiceName name of the header that contains the matched Service in the Ingress
	ServiceName: 'X-Service-Name',

	// ServicePort name of the header that contains the matched Service port in the Ingress
	ServicePort: 'X-Service-Port'
};

const ACCEPTED_FORMATS = new Map([
	['text/plain', 'txt'],
	['application/json', 'json'],
	['text/html', 'html']
]);

// const ACCEPTED_ERRORS = new Map([
// 	['400', 'Bad Request'],
// 	['401', 'Unauthorized'],
// 	['403', 'Forbidden'],
// 	['404', 'Not Found'],
// 	['413', 'Request Entity Too Large'],
// 	['500', 'Internal Server Error'],
// 	['502', 'Bad Gateway'],
// 	['503', 'Service Unavailable'],
// 	['504', 'Gateway Timeout']
// ]);
const ACCEPTED_ERRORS_GERMAN = new Map([
	['400', 'Ungültige Anfrage'],
	['401', 'Unautorisiert'],
	['403', 'Verboten'],
	['404', 'Nicht gefunden'],
	['413', 'Request Entity Too Large'],
	['500', 'Interner Server-Fehler'],
	['502', 'Bad Gateway'],
	['503', 'Dienst nicht verfügbar'],
	['504', 'Gateway Timeout']
]);

router.all('/', (req, res) => {

	// Get format
	let format = req.get(HEADERS.FormatHeader);
	if (format === void 0 || !ACCEPTED_FORMATS.has(format)) {
		format = 'text/html';
	}

	// Set response content type
	res.set(HEADERS.ContentType, format);

	// Get error code
	let errorCode = req.get(HEADERS.CodeHeader);
	if (errorCode === void 0 || !ACCEPTED_ERRORS_GERMAN.has(errorCode)) {
		errorCode = '500';
	}

	// Set response status code
	res.status(parseInt(errorCode));

	// Compute response content
	let content = '';
	switch (format) {
		case 'application/json': {
			content = `{"${errorCode}":"${ACCEPTED_ERRORS_GERMAN.get(errorCode)}"}`;
			break;
		}

		case 'text/plain': {
			content = `${errorCode} - ${ACCEPTED_ERRORS_GERMAN.get(errorCode)}`;
			break;
		}

		case 'text/html':
		default: {
			let html = fs.readFileSync(path.resolve(__dirname, '../errorTemplate.html'), 'utf8');
			html = html.replace(/{{statusCode}}/g, errorCode);
			html = html.replace(/{{statusText}}/g, ACCEPTED_ERRORS_GERMAN.get(errorCode));

			content = html;
			break;
		}
	}

	// Send response
	res.send(content);

});

module.exports = router;