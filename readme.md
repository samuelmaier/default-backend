# "default-backend"
This default backend is meant to run globally (namesapce kube-system) to provide costum error handling for the ingress controller.

# Deployment
To implement the default backend, follow this official example by nginx:
https://github.com/kubernetes/ingress-nginx/tree/master/docs/examples/customization/custom-errors

## Change Log
### 0.0.1
- Supportet status codes: 400, 401, 403, 404, 413, 500, 502, 503, 504
- Log all errors
### 0.1.0
- Update node version to 10.15.3
### 0.2.0
- Add url to morgan log output
### 0.3.0
- Remove internal 404 error handling from default backend (because it overrides the 404 coming from a service provider)
### 1.0.0
- Changing the GET error route to a ALL route
### 1.0.1