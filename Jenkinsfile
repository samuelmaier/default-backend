pipeline {
  agent any
  options {
    buildDiscarder logRotator(numToKeepStr: '50')
  }

  environment {
    NPM_TOKEN = credentials('verdaccio_auth_token')
    APP_NAME = "default-backend"
    IMAGE_NAME = "rrcenter/${APP_NAME}"
  }

  stages {
    stage('Prepare') {
      steps {
        // Login to private npm registry
        npm_login(NPM_TOKEN)

        // Set additioal git global variables
        set_git_globals()
      }
    }
    stage('Install dependencies') {
      steps {
        npm_install()
      }
    }
    stage('Linting') {
      steps {
        linting()
      }
    }
    stage('Unit Tests') {
      steps {
        unittests()
      }
    }
    stage('Build and publish docker image') {
      steps {
        build_and_publish_docker_image()
      }
    }
    stage('Manually deploy to Kubernetes') {
      when { branch 'master' }
      steps {
        echo "Execute the following command on the server to deploy the default-backend:\n\nkubectl patch deployment default-http-backend -n kube-system -p '{\"spec\":{\"template\":{\"spec\":{\"containers\":[{\"name\":\"default-http-backend\",\"image\":\"docker.kubernetes.kernetics.de/${IMAGE_NAME}:${BUILD_NUMBER}\"}]}}}}'"
      }
    }
  }
  post {
    always {
      // Cleanup
      cleanup()
    }
    failure {
      // Inform commiter via mail
      inform_commiter_on_fail()
    }
  }
}