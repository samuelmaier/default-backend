'use strict';
const express = require('express');
const app = express();
const http = require('http');
const compression = require('compression');

const errorRoute = require('./routes/errorRoute');

// Disable `x-powered-by` header field
app.disable('x-powered-by');

// Zip the response
app.use(compression());

// Routes
app.use('/healthz', (req, res) => {
	res.status(200).send();
});

app.use('/', errorRoute);

// Catch 404 and forward to error handler
app.use((req, res, next) => {
	let err = new Error(`Not Found: ${req.path}`);
	err.status = 404;
	next(err);
});

// Error handler
app.use((err, req, res) => {
	res.status(err.status || 500);
	res.send();
	console.error(err);
});

// **********************
// * Start the server.  *
// **********************
const httpServer = http.createServer(app);
httpServer.listen(8080, '0.0.0.0', () =>
	console.log('Server has been started...'));